<?php

use yii\db\Migration;

class m160725_142621_status2_table extends Migration
{
    public function up()
    {
				$this->createTable(
		'status2',
			[
				'id' => 'pk',
				'name' => 'string',
			]
		);
    }

    public function down()
    {
		$this->dropTable('status2');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
